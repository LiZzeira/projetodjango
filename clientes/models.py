from django.db import models


# Create your models here.

class Departamento (models.Model):
    nome = models.CharField(max_length=100, blank=False, null=False)

    def __str__ (self):
        return self.nome


class Cliente(models.Model):
    nome = models.CharField(max_length=70, null=False)
    CPF = models.CharField(max_length=14, null=False)
    endereço = models.CharField(max_length=200, blank=False, null=False)
    salario = models.DecimalField(max_digits=10, decimal_places=2)
    idade = models.IntegerField()
    email = models.EmailField()
    departamentos =  models.ManyToManyField(Departamento, blank=True)
    
    def __str__(self):
       return self.nome


class telefone (models.Model):
    numero = models.CharField(max_length=20)
    descricao = models.CharField(max_length=80)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)

    def __str__(self):
        return 'telefone '+ self.descricao

class PPPoE (models.Model):
    usuario = models.CharField(max_length=32, null=False)
    senha = models.CharField(max_length=32, null=False)
    gpon = models.CharField(max_length=32, null=False)
    mac = models.CharField(max_length=32, null=False)
    usuarios = models.ForeignKey(Cliente, on_delete=models.CASCADE)

    def __str__ (self):
        return self.usuario 

