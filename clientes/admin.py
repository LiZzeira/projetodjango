from django.contrib import admin
from .models import Cliente, telefone, PPPoE, Departamento

# Register your models here.

class ClienteAdmin(admin.ModelAdmin):
    list_display = ('id', 'nome', 'endereço', 'email')
    list_filter = ('departamentos',)
    search_fields = ('id', 'nome', 'endereço', 'email',)


    


admin.site.register(Cliente, ClienteAdmin)
admin.site.register(PPPoE)
admin.site.register(telefone)
admin.site.register(Departamento)
 